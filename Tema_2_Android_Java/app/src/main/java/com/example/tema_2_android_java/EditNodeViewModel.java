package com.example.tema_2_android_java;

import android.app.Application;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

class EditUserViewModel extends AndroidViewModel {

    private String TAG = this.getClass().getSimpleName();
    private UserDao userDao;
    private AppDatabase db;

    public EditUserViewModel(@NonNull Application application) {
        super(application);
        db = AppDatabase.getDatabase(application);
        userDao = db.userDao();
    }

    public void insert(User user)
    {
        new InsertAsyncTask(userDao).execute(user);
    }

    public List<User> getAll()
    {
        return userDao.getAll();
    }

    private class InsertAsyncTask extends AsyncTask <User, Void, Void>
    {

        UserDao mUserDao;

        InsertAsyncTask(UserDao mUserDao) {
            this.mUserDao = mUserDao;
        }

        @Override
        protected Void doInBackground(User... users) {
            //mUserDao.insert(users[0]);
            mUserDao.insertAll(users);
            return null;
        }
    }

//    public LiveData<User> getNote(String noteId) {
//        return noteDao.getNote(noteId);
//    }
}
