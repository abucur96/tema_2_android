package com.example.tema_2_android_java;

import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;
import java.util.StringTokenizer;


/**
 * A simple {@link Fragment} subclass.
 */
public class FirstFragment extends Fragment {

    private Button nextPageBtn;
    private Button addUserBtn;
    private Button removeUserBtn;
    private Button syncWithServer;
    private EditText firstNameEditText;
    private EditText lastNameEditText;

    private AppDatabase appDatabase;
    private UserDao userDao;

    public FirstFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        AppDatabase appDatabase = AppDatabase.getDatabase(getContext());
        UserDao userDao = appDatabase.userDao();
        return inflater.inflate(R.layout.fragment_first, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        firstNameEditText = view.findViewById(R.id.ed_first_name);
        lastNameEditText = view.findViewById(R.id.ed_last_name);

        nextPageBtn = view.findViewById(R.id.btn_next_page);
        addUserBtn = view.findViewById(R.id.btn_save);
        removeUserBtn = view.findViewById(R.id.btn_delete);
        syncWithServer = view.findViewById(R.id.btn_sync_with_server);

        nextPageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToNextFragment();
            }
        });

        addUserBtn.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                addUser();
            }
        });

        removeUserBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                deleteUser();
            }
        });


        syncWithServer.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                parseRequest();
            }
        });
    }

    private void goToNextFragment()
    {
        SecondFragment secondFragment = new SecondFragment();
        Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, secondFragment)
                .addToBackStack("fragment_second")
                .commit();
    }

    private void addUser()
    {
        String firstNameString;
        String lastNameString;


        firstNameString = firstNameEditText.getText().toString();
        lastNameString = lastNameEditText.getText().toString();

        if(firstNameString.isEmpty())
        {
            firstNameEditText.setError("Insert the first name");
        }
        else if(lastNameString.isEmpty())
        {
            firstNameEditText.setError(null);
            lastNameEditText.setError("Insert the last name");
        }
        else
        {
            lastNameEditText.setError(null);
            User newUser = new User(firstNameString, lastNameString);
            new AsyncTaskInsert().execute(newUser);


            firstNameEditText.setText("");
            lastNameEditText.setText("");
        }
    }

    private void deleteUser()
    {
        String firstNameString;
        String lastNameString;


        firstNameString = firstNameEditText.getText().toString();
        lastNameString = lastNameEditText.getText().toString();

        if(firstNameString.isEmpty())
        {
            firstNameEditText.setError("Insert the first name");
        }
        else if(lastNameString.isEmpty())
        {
            firstNameEditText.setError(null);
            lastNameEditText.setError("Insert the last name");
        }
        else
        {
            lastNameEditText.setError(null);

            if(appDatabase == null || userDao == null) {
                appDatabase = AppDatabase.getDatabase(getContext());
                userDao = appDatabase.userDao();
            }

            User user = new User(firstNameString, lastNameString);
            new AsyncTaskDelete().execute(user);

            firstNameEditText.setText("");
            lastNameEditText.setText("");
        }
    }

    private void parseRequest()
    {
        RequestQueue queue = Volley.newRequestQueue(Objects.requireNonNull(getContext()));
        String url ="https://jsonplaceholder.typicode.com/users";

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        ArrayList<User> users = new ArrayList<>();
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            for(int index=0; index < jsonArray.length(); ++index)
                            {
                                if(jsonArray.get(index) instanceof JSONObject)
                                {
                                    JSONObject jsonObject = (JSONObject) jsonArray.get(index);

                                    String name = jsonObject.getString("name");
                                    StringTokenizer tokenizer = new StringTokenizer(name);
                                    User user = new User(tokenizer.nextToken(), tokenizer.nextToken());
                                    users.add(user);
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if(!users.isEmpty())
                        {
                            new AsyncTaskInsert().execute(users.toArray(new User[users.size()]));
                            Toast.makeText(getActivity(), "Sync succeded!",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });


        queue.add(stringRequest);
    }

    private class AsyncTaskInsert extends AsyncTask<User, Void, Void>
    {
        boolean inserted = true;
        @Override
        protected Void doInBackground(User... users) {

            if(appDatabase == null || userDao == null) {
                appDatabase = AppDatabase.getDatabase(getContext());
                userDao = appDatabase.userDao();
            }

            for(User tmpUser: users)
                userDao.insert(tmpUser);

            if(users.length == 0)
                inserted = false;

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(inserted)
                Toast.makeText(getActivity(), "Successfully inserted!",
                        Toast.LENGTH_LONG).show();
        }
    }

    private class AsyncTaskDelete extends AsyncTask<User, Void, Void>
    {
        boolean deleted = true;
        User tempUser;

        @Override
        protected Void doInBackground(User... users) {

            for (User user :
                    users) {
                tempUser = userDao.findByName(user.firstName, user.lastName);

                if(tempUser != null)
                    userDao.delete(tempUser);
                else
                   deleted = false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(!deleted)
                Toast.makeText(getActivity(), "This user doesn't exist!",
                        Toast.LENGTH_LONG).show();
        }
    }
}
