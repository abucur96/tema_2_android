package com.example.tema_2_android_java;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {User.class}, version = 1)
abstract class AppDatabase extends RoomDatabase {
    abstract UserDao userDao();

    private static volatile AppDatabase appDatabaseInstance;

    static AppDatabase getDatabase(final Context context) {
        if (appDatabaseInstance == null) {
            synchronized (AppDatabase.class) {
                if (appDatabaseInstance == null) {
                    appDatabaseInstance = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class, "user_database")
                            //.allowMainThreadQueries()
                            .build();
                }
            }
        }
        return appDatabaseInstance;
    }
}
