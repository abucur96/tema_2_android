package com.example.tema_2_android_java;

import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class SecondFragment extends Fragment {

    private RecyclerView recyclerView;
    private MyAdapter myAdapter;
    private RecyclerView.LayoutManager layoutManager;

    AppDatabase appDatabase;
    UserDao userDao;

    ArrayList<User> usersList;

    public SecondFragment() {}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = (RecyclerView) view.findViewById(R.id.RecyclerViewId);
        updateRecyclerView(recyclerView);
    }

    private void updateRecyclerView(RecyclerView recyclerView)
    {
        new AsyncTaskGetAllUsers().execute();
    }

    private class AsyncTaskGetAllUsers extends AsyncTask<Void, Void, ArrayList<User>>
    {

        @Override
        protected ArrayList<User> doInBackground(Void... voids) {

            if(appDatabase == null || userDao == null) {
                appDatabase = AppDatabase.getDatabase(getContext());
                userDao = appDatabase.userDao();
            }

            usersList = (ArrayList<User>) userDao.getAll();

            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<User> users) {
            super.onPostExecute(users);
            layoutManager = new LinearLayoutManager(getContext());
            recyclerView.setLayoutManager(layoutManager);

            myAdapter = new MyAdapter(usersList);
            recyclerView.setAdapter(myAdapter);
        }
    }
}
