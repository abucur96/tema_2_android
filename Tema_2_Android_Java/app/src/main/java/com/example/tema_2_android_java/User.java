package com.example.tema_2_android_java;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
class User
{

    @PrimaryKey(autoGenerate = true)
    public int uId = 0;

    @ColumnInfo(name = "first_name")
    public String firstName;

    @ColumnInfo(name = "last_name")
    public String lastName;


    User(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    User()
    {
        this.firstName = "";
        this.lastName = "";
    }
}
